import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React from "react";
import Search from "../screens/Search";

const Stack = createNativeStackNavigator();

const SearchStack = (): JSX.Element => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="search-home"
                component={Search}
                options={{ 
                    title: 'Buscar'
                 }}
            />
        </Stack.Navigator>
    )
}

export default SearchStack;