import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React from "react";
import Restaurants from "../screens/Restaurants";

const Stack = createNativeStackNavigator();

const RestaurantsStack = (): JSX.Element => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="restaurants-home"
                component={Restaurants}
                options={{ 
                    title: 'Restaurantes'
                 }}
            />
        </Stack.Navigator>
    )
}

export default RestaurantsStack;