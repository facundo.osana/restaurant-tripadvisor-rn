import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React from "react";
import Account from "../screens/Account/Account";
import Login from "../screens/Account/Login";
import Register from "../screens/Account/Register";
import Routes from "./Routes";

const Stack = createNativeStackNavigator();

const AccountStack = (): JSX.Element => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name={Routes.accountHome}
                component={Account}
                options={{ 
                    title: 'Cuenta'
                 }}
            />
            <Stack.Screen
                name={Routes.accountLogin}
                component={Login}
                options={{ 
                    title: 'Login'
                 }}
            />
            <Stack.Screen
                name={Routes.accountRegister}
                component={Register}
                options={{ 
                    title: 'Registro'
                 }}
            />
        </Stack.Navigator>
    )
}

export default AccountStack;