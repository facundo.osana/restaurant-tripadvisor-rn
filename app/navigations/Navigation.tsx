import React from "react";
import { NavigationContainer, ParamListBase, RouteProp } from "@react-navigation/native";
import { BottomTabNavigationOptions, createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import RestaurantsStack from "./RestaurantsStack";
import FavoritesStack from "./FavoritesStack";
import TopRestaurantsStack from "./TopRestaurantsStack";
import SearchStack from "./SearchStack";
import AccountStack from "./AccountStack";
import ScreenProps from "./ScreenProps";
import Routes from "./Routes";
import { Icon } from "@rneui/base";

const Tab = createBottomTabNavigator();

const Navigation = (): JSX.Element => {
    return (
        <NavigationContainer>
            <Tab.Navigator
                initialRouteName={Routes.restaurantsStack}
                screenOptions={({ route }: ScreenProps): BottomTabNavigationOptions => ({
                    tabBarInactiveTintColor: '#646464',
                    tabBarActiveTintColor: '#00a680',
                    tabBarIcon: ({ color }) => tabBarIcon(route, color)
                })}
            >
                <Tab.Screen
                    name={Routes.restaurantsStack}
                    component={RestaurantsStack}
                    options={{ 
                        title: 'Restaurantes',
                        headerShown: false,
                     }}
                />
                <Tab.Screen
                    name={Routes.favoritesStack}
                    component={FavoritesStack}
                    options= {{ 
                        title: 'Favoritos',
                        headerShown: false,
                     }}
                />
                <Tab.Screen
                    name={Routes.topRestaurantsStack}
                    component={TopRestaurantsStack}
                    options= {{ 
                        title: 'Los mejores',
                        headerShown: false,
                     }}
                />
                <Tab.Screen
                    name={Routes.searchStack}
                    component={SearchStack}
                    options= {{ 
                        title: 'Buscar',
                        headerShown: false,
                     }}
                />
                <Tab.Screen
                    name={Routes.accountStack}
                    component={AccountStack}
                    options= {{ 
                        title: 'Cuenta',
                        headerShown: false,
                     }}
                />
            </Tab.Navigator>
        </NavigationContainer>
    );
}

const tabBarIcon = (route: RouteProp<ParamListBase, string>, color: string): JSX.Element => {
    const iconName: string = {
        [Routes.restaurantsStack]: 'compass-outline',
        [Routes.favoritesStack]: 'heart-outline',
        [Routes.topRestaurantsStack]: 'star-outline',
        [Routes.searchStack]: 'magnify',
        [Routes.accountStack]: 'home-outline',
    }[route.name]

    return (
        <Icon type="material-community" name={iconName} color={color} />
    );
}

export default Navigation;