import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React from "react";
import TopRestaurants from "../screens/TopRestaurants";

const Stack = createNativeStackNavigator();

const TopRestaurantsStack = (): JSX.Element => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="top-restaurants-home"
                component={TopRestaurants}
                options={{ 
                    title: 'Los mejores'
                 }}
            />
        </Stack.Navigator>
    )
}

export default TopRestaurantsStack;