import { NavigationProp, ParamListBase, RouteProp } from '@react-navigation/native';

type ScreenProps = {
    navigation: NavigationProp<ParamListBase>;
    route: RouteProp<ParamListBase, string>;
}

export default ScreenProps;