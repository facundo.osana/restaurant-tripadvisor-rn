type Route = {
    restaurantsStack: string;
    favoritesStack: string;
    topRestaurantsStack: string;
    searchStack: string;
    accountStack: string;

    accountHome: string;
    accountLogin: string;
    accountRegister: string;
}

const Routes: Route = {
    restaurantsStack: 'restaurants-stack',
    favoritesStack: 'favorites-stack',
    topRestaurantsStack: 'topRestaurants-stack',
    searchStack: 'search-stack',
    accountStack: 'account-stack',

    accountHome: 'account-home',
    accountLogin: 'account-login',
    accountRegister: 'account-register',
}

export default Routes;