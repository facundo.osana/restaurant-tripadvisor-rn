import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React from "react";
import Favorites from "../screens/Favorites";

const Stack = createNativeStackNavigator();

const FavoritesStack = (): JSX.Element => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="favorites-home"
                component={Favorites}
                options={{ 
                    title: 'Favoritos'
                 }}
            />
        </Stack.Navigator>
    )
}

export default FavoritesStack;