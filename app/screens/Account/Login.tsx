import { NavigationProp, ParamListBase, useNavigation } from "@react-navigation/native";
import { Divider, Image, Text } from "@rneui/base";
import React from "react";
import { ScrollView, View } from "react-native";
import LoginForm from "../../components/Account/Login/LoginForm";
import Routes from "../../navigations/Routes";
import styles from "./styles/LoginStyles";

const Login = (): JSX.Element => {
    const navigation: NavigationProp<ParamListBase> = useNavigation();

    const createAccount = (): JSX.Element => {
        return (
            <Text style={styles.textRegister}>
                ¿Aún no tienes una cuenta?{" "}
                <Text
                    style={styles.btnRegister}
                    onPress={() => {navigation.navigate(Routes.accountRegister)}}
                >Registate</Text>
            </Text>
        );
    }

    return (
        <ScrollView>
            <Image
                source={require('../../assets/img/app-logo.png')}
                resizeMode="contain"
                style={styles.logo}
            />
            <View style={styles.viewContainer}>
                <LoginForm />
                {createAccount()}
            </View>
        </ScrollView>
    )
}

export default Login;