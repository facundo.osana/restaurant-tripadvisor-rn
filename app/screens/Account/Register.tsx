import { Image, Text } from "@rneui/base";
import React from "react";
import { View } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import RegisterForm from "../../components/Account/Register/RegisterForm";
import styles from "./styles/RegisterStyles";

const Register = (): JSX.Element => {
    return (
        <KeyboardAwareScrollView>
            <Image
                source={require('../../assets/img/app-logo.png')}
                resizeMode="contain"
                style={styles.logo}
            />
            <View
                style={styles.viewForm}
            >
                <RegisterForm />
            </View>
        </KeyboardAwareScrollView>
    )
}

export default Register;