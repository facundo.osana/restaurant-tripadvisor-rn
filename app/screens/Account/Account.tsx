import React, { useEffect, useState } from "react";
import { Auth, getAuth, User } from "firebase/auth";
import UserLogged from "./UserLogged";
import UserGuest from "./UserGuest";
import Loader from "../../components/Loader";

const Account = (): JSX.Element => {
    const [userLogged, setUserLogged] = useState<boolean>(false);
    const [loading, setLoading] = useState<boolean>(true);

    useEffect(() => {
        const auth: Auth = getAuth();
        auth.onAuthStateChanged((user: User | null) => {
            !user ? setUserLogged(false) : setUserLogged(true);
            
            setTimeout(() => {  // Feels like Ángel :'v
                setLoading(false);
            }, 3000)
        })
    }, []);

    if (loading)
        return <Loader />

    return userLogged ? <UserLogged /> : <UserGuest />
}

export default Account;