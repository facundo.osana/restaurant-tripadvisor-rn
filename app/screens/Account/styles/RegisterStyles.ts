import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    logo: {
        width: "100%",
        height: 150,
        marginTop: 20,
    },
    viewForm: {
        marginHorizontal: 40,
    }
})

export default styles;