import { Text } from "@rneui/base";
import React from "react";

const UserLogged = (): JSX.Element => {
    return (
        <>
            <Text>User Logged...</Text>
        </>
    );
}

export default UserLogged;