import { NavigationProp, ParamListBase, useNavigation } from "@react-navigation/native";
import { Button, Text } from "@rneui/base";
import React from "react";
import { Image, ScrollView, View } from "react-native";
import Routes from "../../navigations/Routes";
import styles from "./styles/UserGuestStyles";

const UserGuest = (): JSX.Element => {
    const navigation: NavigationProp<ParamListBase> = useNavigation();

    return (
        <ScrollView centerContent={true} style={styles.viewBody}>
            <Image
                source={require("../../assets/img/user-guest.jpg")}
                resizeMode="contain"
                style={styles.image}
            />
            <Text style={styles.title}>Consulta tu perfil de 5 Tenedores</Text>
            <Text style={styles.description}>
                ¿Cómo describirías tu mejor restaurante? Buscá y visualizá los mejores
                restaurantes de una forma sencilla, votá cual te ha gustado más y
                comentá como ha sido tu experiencia.
            </Text>
            <View style={styles.btnView}>
                <Button
                    title="Ver tu perfil"
                    buttonStyle={styles.btn}
                    containerStyle={styles.btnContainer}
                    onPress={() => {navigation.navigate(Routes.accountLogin)}}
                />
            </View>
        </ScrollView>
    );
}

export default UserGuest;