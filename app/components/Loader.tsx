import React, { useEffect, useRef } from "react";
import AnimatedLottieView from "lottie-react-native";

const Loader = (): JSX.Element => {
    const loadingAnimationRef = useRef<AnimatedLottieView>(null)

    useEffect(() => {
        loadingAnimationRef.current?.play();
    }, []);

    return <AnimatedLottieView
        source={require("../assets/loader/loading.json")}
        ref={loadingAnimationRef}
    />
}

export default Loader;