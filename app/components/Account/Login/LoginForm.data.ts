import * as Yup from 'yup';

export type initialValuesTypes = {
    email: string;
    password: string;
}

const initialValues: initialValuesTypes = {
    email: "",
    password: "",
}

const validationSchema = () => {
    return Yup.object({
        email: Yup.string()
            .email('El email no es válido')
            .required('El email es obligatorio'),
        password: Yup.string()
            .required('La contraseña es obligatoria'),
    });
}

export { validationSchema, initialValues };