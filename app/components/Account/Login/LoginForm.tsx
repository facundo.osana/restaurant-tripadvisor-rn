import { Button, Icon, Input } from "@rneui/themed";
import { Formik, FormikProps } from "formik";
import React, { useRef, useState } from "react";
import { TextInput, View } from "react-native";
import { initialValues, initialValuesTypes, validationSchema } from "./LoginForm.data";
import styles from "./LoginForm.styles";
import Toast from 'react-native-toast-message';
import { Auth, getAuth, signInWithEmailAndPassword } from "firebase/auth";
import { NavigationProp, ParamListBase, useNavigation } from "@react-navigation/native";
import Routes from "../../../navigations/Routes";

const LoginForm = (): JSX.Element => {
    const [passwordVisible, setPasswordVisible] = useState<boolean>(false);

    const passwordInput = useRef<TextInput|null>(null);

    const changePasswordVisibility = () => {
        setPasswordVisible(!passwordVisible);
        passwordInput.current?.focus();
    }

    const navigation: NavigationProp<ParamListBase> = useNavigation();

    const onSubmit = async (values: initialValuesTypes) => {
        try {
            const auth: Auth = getAuth();
            const { email, password } = values;
            await signInWithEmailAndPassword(auth, email, password);
            navigation.navigate(Routes.accountHome);
        } catch (error) {
            Toast.show({
                type: 'error',
                position: 'bottom',
                text1: 'Usuario o contraseña incorrectos'
            })
        }
    }

    return (
        <View
            style={styles.content}
        >
            <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                validateOnChange={false}
                onSubmit={onSubmit}
            >
                {({ handleChange, handleSubmit, isSubmitting, errors }: FormikProps<initialValuesTypes>) => (
                    <>
                        <Input
                            placeholder="Correo electrónico"
                            shake={() => {}}
                            containerStyle={styles.input}
                            rightIcon={<Icon
                                type="material-community"
                                name="at"
                                iconStyle={styles.icon}
                            />}
                            onChangeText={handleChange('email')}
                            errorMessage={errors.email}
                            disabled={isSubmitting}
                            onSubmitEditing={() => {passwordInput.current?.focus()}}
                            blurOnSubmit={false}
                        />
                        <Input
                            placeholder="Contraseña"
                            shake={() => {}}
                            containerStyle={styles.input}
                            secureTextEntry={!passwordVisible}
                            rightIcon={<Icon
                                type="material-community"
                                name={passwordVisible ? "eye-off-outline" : "eye-outline"}
                                iconStyle={styles.icon}
                                onPress={changePasswordVisibility}
                            />}
                            onChangeText={handleChange('password')}
                            errorMessage={errors.password}
                            disabled={isSubmitting}
                            ref={passwordInput}
                            onSubmitEditing={handleSubmit}
                        />
                        <Button
                            containerStyle={styles.btnContainer}
                            buttonStyle={styles.btn}
                            title="Iniciar sesión"
                            onPress={handleSubmit}
                            loading={isSubmitting}
                        />
                    </>
                )}
            </Formik>
        </View>
    )
}

export default LoginForm;