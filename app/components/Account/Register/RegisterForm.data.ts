import * as Yup from "yup";

export type initialValuesTypes = {
    email: string;
    password: string;
    repeatPassword: string;
}

const initialValues: initialValuesTypes = {
    email: "",
    password: "",
    repeatPassword: "",
}

const validationSchema = () => {
    return Yup.object({
        email: Yup.string()
            .email('El email no es válido')
            .required('El email es obligatorio'),
        password: Yup.string()
            .required('La contraseña es obligatoria'),
        repeatPassword: Yup.string()
            .oneOf([Yup.ref('password')], 'Las contraseñas no coinciden'),
    });
}

export { validationSchema, initialValues };