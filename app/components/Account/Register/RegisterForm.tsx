import { NavigationProp, ParamListBase, useNavigation } from "@react-navigation/native";
import { Button, Icon, Input } from "@rneui/themed";
import { Auth, createUserWithEmailAndPassword, getAuth } from "firebase/auth";
import { Formik, FormikProps } from "formik";
import React, { useRef, useState } from "react";
import { TextInput, View } from "react-native";
import Routes from "../../../navigations/Routes";
import styles from "./RegisterForm.styles";
import { initialValues, initialValuesTypes, validationSchema } from "./RegisterForm.data";
import Toast from 'react-native-toast-message'

const RegisterForm = (): JSX.Element => {
    const [passwordVisible, setPasswordVisible] = useState<boolean>(false);
    const [repeatPasswordVisible, setRepeatPasswordVisible] = useState<boolean>(false);

    const passwordInput = useRef<TextInput|null>(null)
    const repeatPasswordInput = useRef<TextInput|null>(null)

    const navigation: NavigationProp<ParamListBase> = useNavigation();

    const changePasswordVisibility = () => {
        setPasswordVisible(!passwordVisible);
        passwordInput.current?.focus();
    }

    const changeRepeatPasswordVisibility = () => {
        setRepeatPasswordVisible(!repeatPasswordVisible);
        repeatPasswordInput.current?.focus();
    }

    const onSubmit = async (values: initialValuesTypes) => {
        try {
            const auth: Auth = getAuth();
            const { email, password } = values;
            await createUserWithEmailAndPassword(auth, email, password);
            navigation.navigate(Routes.accountHome);
        } catch (error) {
            Toast.show({
                type: 'error',
                position: 'bottom',
                text1: 'Error al registrarse, intente más tarde'
            })
        }
    }

    return (
        <View style={styles.formContainer}>
            <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                validateOnChange={false}
                onSubmit={onSubmit}
            >
                {({ handleChange, handleSubmit, isSubmitting, errors }: FormikProps<initialValuesTypes>) => (
                    <>
                        <Input
                            placeholder="Correo electronico"
                            containerStyle={styles.inputForm}
                            shake={() => {}}
                            rightIcon={<Icon
                                type="material-community"
                                name="at"
                                iconStyle={styles.inputIcon}
                            />}
                            onChangeText={handleChange('email')}
                            errorMessage={errors.email}
                            disabled={isSubmitting}
                            onSubmitEditing={() => {passwordInput.current?.focus()}}
                            blurOnSubmit={false}
                        />
                        <Input
                            placeholder="Contraseña"
                            containerStyle={styles.inputForm}
                            shake={() => {}}
                            secureTextEntry={!passwordVisible}
                            rightIcon={<Icon
                                type="material-community"
                                name={passwordVisible ? "eye-off-outline" : "eye-outline"}
                                iconStyle={styles.inputIcon}
                                onPress={changePasswordVisibility}
                            />}
                            onChangeText={handleChange('password')}
                            errorMessage={errors.password}
                            disabled={isSubmitting}
                            ref={passwordInput}
                            onSubmitEditing={() => {repeatPasswordInput.current?.focus()}}
                            blurOnSubmit={false}
                        />
                        <Input
                            placeholder="Repetir contraseña"
                            containerStyle={styles.inputForm}
                            shake={() => {}}
                            secureTextEntry={!repeatPasswordVisible}
                            rightIcon={<Icon
                                type="material-community"
                                name={repeatPasswordVisible ? "eye-off-outline" : "eye-outline"}
                                iconStyle={styles.inputIcon}
                                onPress={changeRepeatPasswordVisibility}
                            />}
                            onChangeText={handleChange('repeatPassword')}
                            errorMessage={errors.repeatPassword}
                            disabled={isSubmitting}
                            ref={repeatPasswordInput}
                            onSubmitEditing={handleSubmit}
                        />
                        <Button
                            title="Unirse"
                            containerStyle={styles.btnContainerRegister}
                            buttonStyle={styles.btnRegister}
                            onPress={handleSubmit}
                            loading={isSubmitting}
                        />
                    </>
                )}
            </Formik>
        </View>
    )
}

export default RegisterForm;