import React from 'react';
import { LogBox } from 'react-native';
import Navigation from './app/navigations/Navigation';
import Toast from 'react-native-toast-message'

LogBox.ignoreAllLogs();

const App = (): JSX.Element => {
    return (
        <>
            <Navigation />
            <Toast />
        </>
    )
}

export default App;