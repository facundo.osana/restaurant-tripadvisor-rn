/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import FirebaseApp from './app/utils/Firebase';

FirebaseApp;
AppRegistry.registerComponent(appName, () => App);
