module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    ["module:react-native-dotenv", {
      "envName": "ENV",
      "moduleName": "@env",
      "path": ".env",
      "safe": true,
      "allowUndefined": true,
      "verbose": false
    }]
  ]
};
